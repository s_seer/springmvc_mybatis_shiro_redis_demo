CREATE TABLE demo(
  `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'id',
  `name` VARCHAR(20) DEFAULT null COMMENT '名称',
  `desc` VARCHAR(50) DEFAULT null COMMENT '描述'
) COMMENT '测试表';

#------shiro
#--创建用户表
create table sys_users (
  id bigint auto_increment COMMENT 'id',
  username varchar(100) COMMENT '用户名',
  password varchar(100) COMMENT '密码',
  salt varchar(100) COMMENT '盐',
  locked bool default false COMMENT '是否锁定',
  constraint pk_sys_users primary key(id)
) charset=utf8 ENGINE=InnoDB COMMENT='用户表';
create unique index idx_sys_users_username on sys_users(username);

#--创建角色表
create table sys_roles (
  id bigint auto_increment COMMENT 'id',
  role varchar(100) COMMENT '角色标识',
  description varchar(100) COMMENT '角色描述',
  available bool default false COMMENT '是否可用,如果不可用将不会添加给用户',
  constraint pk_sys_roles primary key(id)
) charset=utf8 ENGINE=InnoDB COMMENT='角色表';
create unique index idx_sys_roles_role on sys_roles(role);

#--创建权限表
create table sys_permissions (
  id bigint auto_increment COMMENT 'id',
  permission varchar(100) COMMENT '权限标识',
  description varchar(100) COMMENT '权限描述',
  available bool default false COMMENT '是否可用,如果不可用将不会添加给用户',
  constraint pk_sys_permissions primary key(id)
) charset=utf8 ENGINE=InnoDB COMMENT='权限表';
create unique index idx_sys_permissions_permission on sys_permissions(permission);

#--创建用户角色关系表
create table sys_users_roles (
  user_id bigint COMMENT '用户id',
  role_id bigint COMMENT '角色id',
  constraint pk_sys_users_roles primary key(user_id, role_id)
) charset=utf8 ENGINE=InnoDB COMMENT='用户角色关系表';

#--角色权限关系表
create table sys_roles_permissions (
  role_id bigint COMMENT '角色id',
  permission_id bigint COMMENT '权限id',
  constraint pk_sys_roles_permissions primary key(role_id, permission_id)
) charset=utf8 ENGINE=InnoDB COMMENT='角色权限关系表';
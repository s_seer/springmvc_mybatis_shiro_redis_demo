package com.demo.util;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Spring-test基类
 * 加载spring配置文件
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath*:conf/spring-mvc.xml",
        "classpath*:conf/spring-mybatis.xml",
        "classpath*:conf/spring-shiro.xml"})
public class BaseJunit4Test {
}

package com.demo.test;


import com.demo.model.PO.SysUserPO;
import com.demo.service.UserService;
import com.demo.util.BaseJunit4Test;
import com.sun.xml.internal.rngom.digested.DDefine;
import org.apache.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.LockedAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.SimpleByteSource;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;


public class TestDemo extends BaseJunit4Test {
    private static final Logger LOGGER = Logger.getLogger(TestDemo.class);

    @Autowired
    UserService userService;

    @Test
    public void test() {

    }

    @Test
    /**
     * 测试注册
     */
    public void testReg() {
        LOGGER.info("... . . .-. 增加用户");
        SysUserPO sysUserPO = new SysUserPO();
        sysUserPO.setUsername("see");
        sysUserPO.setPassword("111111");
        sysUserPO.setSalt("666");

        /**
         String algorithmName = "md5";
         String username = "liu";
         String password = "123";
         String salt1 = username;
         String salt2 = new SecureRandomNumberGenerator().nextBytes().toHex();
         int hashIterations = 2;

         SimpleHash hash = new SimpleHash(algorithmName, password, salt1 + salt2, hashIterations);
         String encodedPassword = hash.toHex();
         如果要写用户模块，需要在新增用户/重置密码时使用如上算法保存密码，将生成的密码及salt2存入数据库（因为我们的散列算法是：md5(md5(密码+username+salt2))）。
         */

        SimpleHash hash = new SimpleHash("MD5", sysUserPO.getPassword(), sysUserPO.getUsername() + sysUserPO.getSalt(), 2);
        String encodedPwd = hash.toHex();
        sysUserPO.setPassword(encodedPwd);

        userService.register(sysUserPO);
    }

    @Test
    /**
     * 测试登录
     */
    public void testLogin() {
        Subject subject = SecurityUtils.getSubject();
        if (subject.isAuthenticated()) {
            LOGGER.info("... . . .-. 已经登陆，直接进入");
        }

        UsernamePasswordToken token = new UsernamePasswordToken("see", "111111");
        try {
            subject.login(token);
            LOGGER.info("... 登录成功");
        } catch (LockedAccountException e) {
            LOGGER.error("... . . .-. 帐号锁定");
        } catch (AuthenticationException e) {
            LOGGER.error("... . . .-. 用户名或密码错误");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

<%--
  Created by IntelliJ IDEA.
  User: seer
  Date: 2017/6/9
  Time: 15:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%String path = request.getContextPath();%>
    <meta charset="UTF-8">
    <title>管理系统 v1.0</title>
    <link rel="stylesheet" href="<%=path%>/js/bootstrap/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="<%=path%>/js/bootstrap/css/bootstrap-theme.min.css" type="text/css">
    <!--font-awesome 核心CSS 文件-->
    <link href="//cdn.bootcss.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <style type="text/css">
        body {
            background: url("<%=path%>/image/background.jpg") no-repeat;
            background-size: cover;
            font-size: 16px;
        }

        .form {
            background: rgba(255, 255, 255, 0.2);
            width: 400px;
            margin: 200px auto;
        }

        .fa {
            display: inline-block;
            top: 27px;
            left: 6px;
            position: relative;
            color: #cccccc;
        }

        input[type="text"], input[type="password"] {
            padding-left: 26px;
        }

        .checkbox {
            padding-left: 21px;
        }

        .form-title {
            font-family: 黑体;
            color: #ffffff
        }

        #div_login {
            display: block;
        }

        #div_reg {
            display: none;
        }

        a {
            color: #ffffff;
            text-decoration: underline;
        }

        .error {
            color: #FF0000;
            font-size: small;
        }

    </style>
</head>
<body>
<!--
         基础知识：
         网格系统:通过行和列布局
         行必须放在container内
         手机用col-xs-*
         平板用col-sm-*
         笔记本或普通台式电脑用col-md-*
         大型设备台式电脑用col-lg-*
         为了兼容多个设备，可以用多个col-*-*来控制；
     -->
<div class="container">
    <div class="form row" id="div_login">
        <form class="form-horizontal col-sm-offset-3 col-md-offset-3" id="form_login">
            <h3 class="form-title">用户登录</h3>
            <div class="col-sm-9 col-md-9">
                <div class="form-group">
                    <i class="fa fa-user fa-lg"></i>
                    <input class="form-control required" type="text" placeholder="用户名" name="username" id="username"
                           autofocus="autofocus" maxlength="20"/>
                </div>
                <div class="form-group">
                    <i class="fa fa-lock fa-lg"></i>
                    <input class="form-control required" type="password" placeholder="密码" name="password" id="password"
                           maxlength="8"/>
                </div>
                <div class="form-group">
                    <label class="checkbox">
                        <input type="checkbox" name="remeber" id="remeber" value="1">记住密码</label>
                    </label>
                    <hr/>
                </div>
                <a href="javascript:;" id="goReg" class="">新用户注册</a>
                <div class="from-group">
                    <input type="submit" class="btn btn-success pull-right" style="margin-bottom: 10px;" value="登录">
                </div>
            </div>
        </form>
    </div>

    <div class="form row" id="div_reg">
        <form class="form-horizontal col-sm-offset-3 col-md-offset-3" id="form_reg">
            <h3 class="form-title">用户注册</h3>
            <div class="col-sm-9 col-md-9">
                <div class="form-group">
                    <i class="fa fa-user fa-lg"></i>
                    <input class="form-control" type="text" placeholder="用户名" name="username"
                           id="username_reg"
                           autofocus="autofocus">
                </div>
                <div class="form-group">
                    <i class="fa fa-lock fa-lg"></i>
                    <input class="form-control" type="password" placeholder="密码" name="password"
                           id="password_reg">
                </div>
                <div class="form-group">
                    <i class="fa fa-envelope fa-lg"></i>
                    <input class="form-control" type="text" placeholder="邮箱" name="email" id="email_reg">
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success pull-right" value="注册" onclick="registe()"/>
                    <input type="button" class="btn btn-info pull-left" id="back_btn" value="取消" onclick="cancelReg()"/>
                </div>

            </div>
        </form>
    </div>
</div>
<script type="text/javascript" src="<%=path%>/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript" src="<%=path%>/js/jquery.mloading/jquery.mloading.js"></script>

<script type="text/javascript">
    $("#goReg").click(function () {
        $("#div_login").css("display", "none");
        $("#form_reg")[0].reset();
        $("#div_reg").css("display", "block");
    })

    function cancelReg() {
        $("#div_login").css("display", "block");
        $("#form_login")[0].reset();
        $("#div_reg").css("display", "none");
    }


    function registe() {
        $('#form_reg').validate({
            /*//            错误信息位置
             errorPlacement: function (error, element) {
             error.append(element.parent().next())
             },*/

            rules: {
                username: {
                    required: true
                },
                password: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                username: {
                    required: "用户名不能为空"
                },
                password: {
                    required: "密码不能为空"
                },
                email: {
                    required: "邮箱不能为空",
                    email: "邮箱地址不正确"
                }
            },
            errorClass: "error",
            submitHandler: function () {
//                验证通过，注册
                ajaxReg();
            }
        })
    }

    function ajaxReg() {
        $("body").mLoading("show");
        $.ajax({
            url: "<%=path%>/user/reg.do",
            type: "POST",
            data: $("#form_reg").serialize(),
            success: function (resp) {
                $("body").mLoading("hide");
            },
            fail: function (resp) {
                $("body").mLoading("hide");
            }
        })
    }


</script>

<script type="text/javascript" color="255,255,255" opacity='0.7' zIndex="-2" count="99"
        src="<%=path%>/js/canvas-nest.js"></script>
</body>
</html>

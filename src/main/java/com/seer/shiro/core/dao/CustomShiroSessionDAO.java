package com.seer.shiro.core.dao;

import com.seer.shiro.core.session.ShiroSessionRepository;
import org.apache.log4j.Logger;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.eis.AbstractSessionDAO;

import java.io.Serializable;
import java.util.Collection;

/**
 * 用户session操作
 *
 * @author seer
 * @date 2017/10/9 21:27
 */
public class CustomShiroSessionDAO extends AbstractSessionDAO {
    private static Logger LOGGER = Logger.getLogger(CustomShiroSessionDAO.class);

    private ShiroSessionRepository shiroSessionRepository;

    public ShiroSessionRepository getShiroSessionRepository() {
        return shiroSessionRepository;
    }

    public void setShiroSessionRepository(ShiroSessionRepository shiroSessionRepository) {
        this.shiroSessionRepository = shiroSessionRepository;
    }

    @Override
    protected Serializable doCreate(Session session) {
        Serializable sessionId = this.generateSessionId(session);
        this.assignSessionId(session, sessionId);
        getShiroSessionRepository().saveSession(session);
        return sessionId;
    }

    @Override
    protected Session doReadSession(Serializable sessionId) {
        return getShiroSessionRepository().getSession(sessionId);
    }

    @Override
    public void update(Session session) throws UnknownSessionException {
        getShiroSessionRepository().saveSession(session);
    }

    @Override
    public void delete(Session session) {
        if (null == session) {
            LOGGER.error("session can't is null");
            return;
        }
        Serializable sessionId = session.getId();
        if (null != sessionId) {
            getShiroSessionRepository().deleteSession(sessionId);
        }
    }

    @Override
    public Collection<Session> getActiveSessions() {
        return getShiroSessionRepository().getAllSession();
    }
}

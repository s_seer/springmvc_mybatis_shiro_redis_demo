package com.seer.shiro.core.cache;

import org.apache.shiro.cache.Cache;

/**
 * shiro缓存管理接口
 * @author seer
 * @date 2017/9/19 17:28
 */
public interface ShiroCacheManager {
    <K, V> Cache<K, V> getCache(String name);

    void destory();
}

package com.seer.shiro.core.cache.impl;

import com.seer.shiro.core.cache.JedisManager;
import com.seer.shiro.core.cache.JedisShiroCache;
import com.seer.shiro.core.cache.ShiroCacheManager;
import org.apache.shiro.cache.Cache;

/**
 * shiro缓存管理类
 * @author seer
 * @date 2017/9/19 17:27
 */
public class JedisShiroCacheManager implements ShiroCacheManager {
    private JedisManager jedisManager;

    public JedisManager getJedisManager() {
        return jedisManager;
    }

    public void setJedisManager(JedisManager jedisManager) {
        this.jedisManager = jedisManager;
    }

    @Override
    public <K, V> Cache<K, V> getCache(String name) {
        return new JedisShiroCache<K, V>(jedisManager, name);
    }

    @Override
    public void destory() {
        // 不能关闭
    }
}

package com.seer.shiro.core.cache;

import com.seer.shiro.core.session.impl.JedisShiroSessionRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.shiro.session.Session;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import redis.clients.jedis.Jedis;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * redis操作类
 *
 * @author seer
 * @date 2017/9/19 17:28
 */
public class JedisManager {
    private JedisConnectionFactory jedisConnectionFactory;
    private RedisConnection redisConnection;

    public JedisConnectionFactory getJedisConnectionFactory() {
        return jedisConnectionFactory;
    }

    public void setJedisConnectionFactory(JedisConnectionFactory jedisConnectionFactory) {
        this.jedisConnectionFactory = jedisConnectionFactory;
    }

    /**
     * 获取jedis
     *
     * @return
     * @throws Exception
     */
    public Jedis getJedis() throws Exception {
        if (redisConnection == null) {
            redisConnection = getJedisConnectionFactory().getConnection();
        }
        return (Jedis) redisConnection.getNativeConnection();
    }

    public void closeConnecion(Jedis jedis) {
        if (jedis != null) {
            jedis.close();
        }
        if (redisConnection != null) {
            redisConnection.close();
        }
    }

    /**
     * 查询
     *
     * @param dbIndex
     * @param key
     * @return
     * @throws Exception
     */
    public byte[] getValueByKey(int dbIndex, byte[] key) throws Exception {
        Jedis jedis = null;
        byte[] result;

        try {
            jedis = getJedis();
            jedis.select(dbIndex);
            result = jedis.get(key);
        } catch (Exception e) {
            throw e;
        } finally {
            closeConnecion(jedis);
        }
        return result;
    }

    /**
     * 删除
     *
     * @param dbIndex
     * @param key
     * @return
     * @throws Exception
     */
    public Long deleteByKey(int dbIndex, byte[] key) throws Exception {
        Jedis jedis = null;
        Long result;
        try {
            jedis = getJedis();
            jedis.select(dbIndex);
            result = jedis.del(key);
        } catch (Exception e) {
            throw e;
        } finally {
            closeConnecion(jedis);
        }
        return result;
    }

    /**
     * 存储
     *
     * @param dbIndex
     * @param key
     * @param value
     * @param expireTime
     * @return
     * @throws Exception
     */
    public void saveValueByKey(int dbIndex, byte[] key, byte[] value, int expireTime) throws Exception {
        Jedis jedis = null;
        Long result;
        try {
            jedis = getJedis();
            jedis.select(dbIndex);
            jedis.set(key, value);
            // 给key设置生存时间 (s)
            if (expireTime > 0) {
                jedis.expire(key, expireTime);
            }
        } catch (Exception e) {
            throw e;
        } finally {
            closeConnecion(jedis);
        }
    }

    /**
     * 获取所有的session
     *
     * @param dbIndex
     * @return
     * @throws Exception
     */
    public Collection<Session> allSession(int dbIndex) throws Exception {
        Jedis jedis = null;
        Set<Session> sessionSet = new HashSet<>();
        try {
            jedis = getJedis();
            jedis.select(dbIndex);

            Set<byte[]> keyBytes = jedis.keys((JedisShiroSessionRepository.REDIS_SHIRO_SESSION + "*").getBytes());
            if (!CollectionUtils.isEmpty(keyBytes)) {
                for (byte[] bs : keyBytes) {
                    Session obj = SerializationUtils.deserialize(jedis.get(bs));
                    if (obj instanceof Session) {
                        sessionSet.add(obj);
                    }
                }
            }
        } catch (Exception e) {
            throw e;
        } finally {
            closeConnecion(jedis);
        }
        return sessionSet;
    }
}

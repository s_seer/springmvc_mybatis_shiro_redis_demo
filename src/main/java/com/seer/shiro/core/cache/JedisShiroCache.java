package com.seer.shiro.core.cache;

import org.apache.commons.lang3.SerializationUtils;
import org.apache.log4j.Logger;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;

/**
 * shiro缓存
 * @author seer
 * @date 2017/9/19 17:28
 */
public class JedisShiroCache<K, V> implements Cache<K, V> {
    private static Logger LOGGER = Logger.getLogger(JedisShiroCache.class);
    /**
     * 加前缀，以区分其他缓存
     */
    private static final String REDIS_SHIRO_CACHE = "com-seer-shiro-cache:";

    /**
     * redis分片
     */
    private static final int DB_INDEX = 1;

    private JedisManager jedisManager;

    private String name;

    public JedisShiroCache(JedisManager jedisManager, String name) {
        this.jedisManager = jedisManager;
        this.name = name;
    }

    public String getName() {
        return name == null ? "" : name;
    }

    public void setName(String name) {
        this.name = name;
    }

    /**
     * 构建缓存的key
     *
     * @param key
     * @return
     */
    private String buildCacheKey(Object key) {
        return REDIS_SHIRO_CACHE + getName() + ":" + key;
    }

    @Override
    public Object get(Object key) throws CacheException {
        byte[] byteKey = SerializationUtils.serialize(buildCacheKey(key));
        byte[] byteValue = new byte[0];
        try {
            byteValue = jedisManager.getValueByKey(DB_INDEX, byteKey);
        } catch (Exception e) {
            LOGGER.error("获取shiro缓存异常", e);
        }
        return SerializationUtils.deserialize(byteValue);
    }

    @Override
    public Object put(Object key, Object value) throws CacheException {
        Object previos = get(key);
        try {
            jedisManager.saveValueByKey(DB_INDEX, SerializationUtils.serialize(buildCacheKey(key)), SerializationUtils.serialize((Serializable) value), -1);
        } catch (Exception e) {
            LOGGER.error("存储shiro缓存异常", e);
        }
        return previos;
    }

    @Override
    public Object remove(Object key) throws CacheException {
        Object previos = get(key);
        try {
            jedisManager.deleteByKey(DB_INDEX, SerializationUtils.serialize(buildCacheKey(key)));
        } catch (Exception e) {
            LOGGER.error("删除shiro缓存异常", e);
        }
        return previos;
    }

    @Override
    public void clear() throws CacheException {
    }

    @Override
    public int size() {
        if (keys() == null) {
            return 0;
        }
        return keys().size();
    }

    @Override
    public Set keys() {
        return null;
    }

    @Override
    public Collection values() {
        return null;
    }
}

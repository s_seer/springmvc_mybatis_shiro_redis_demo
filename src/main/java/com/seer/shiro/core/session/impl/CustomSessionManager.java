package com.seer.shiro.core.session.impl;

import com.demo.model.PO.UUser;
import com.seer.shiro.core.bean.bo.UserOnlineBO;
import com.seer.shiro.core.dao.CustomShiroSessionDAO;
import com.seer.shiro.core.session.ShiroSessionRepository;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.apache.shiro.subject.support.DefaultSubjectContext;
import sun.reflect.misc.ConstructorUtil;

import java.util.*;

public class CustomSessionManager {

    /**
     * 在线状态
     */
    public static final String SESSION_STATUS = "com-seer-online-status";

    ShiroSessionRepository shiroSessionRepository;

    CustomShiroSessionDAO customShiroSessionDAO;

    /**
     * 获取所有有效的session用户
     *
     * @return
     */
    public List<UserOnlineBO> listAllUser() {
        // 获取所有session
        Collection<Session> sessions = customShiroSessionDAO.getActiveSessions();
        List<UserOnlineBO> onlineBOList = new ArrayList<>();
        for (Session session : sessions) {
            UserOnlineBO bo = getSessionBo(session);
            if (null != bo) {
                onlineBOList.add(bo);
            }
        }
        return onlineBOList;
    }

    /**
     * 根据ID查询 SimplePrincipalCollection
     *
     * @param userIds
     * @return
     */
    public List<SimplePrincipalCollection> getSimplePrincipalCollectionByUserId(Long... userIds) {
        Set<Long> idSet = new TreeSet<>();
        CollectionUtils.addAll(idSet, userIds);

        // 获取所有的session
        Collection<Session> sessions = customShiroSessionDAO.getActiveSessions();

        List<SimplePrincipalCollection> list = new ArrayList<>();
        for (Session session : sessions) {
            Object obj = session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);

            if (obj instanceof SimplePrincipalCollection) {
                SimplePrincipalCollection spc = (SimplePrincipalCollection) obj;

                obj = spc.getPrimaryPrincipal();
                if (null != obj && obj instanceof UUser) {
                    UUser user = (UUser) obj;
                    // 比较用户Id，符合，加入集合
                    if (null != user && idSet.contains(user.getId())) {
                        list.add(spc);
                    }
                }
            }
        }
        return list;
    }

    /**
     * 获取单个session
     *
     * @param sessionId
     * @return
     */
    public UserOnlineBO getSession(String sessionId) {
        Session session = shiroSessionRepository.getSession(sessionId);
        return getSessionBo(session);
    }


    /**
     * 获取session中的对象
     *
     * @param session
     * @return
     */
    private UserOnlineBO getSessionBo(Session session) {
        // 获取session登录信息
        Object obj = session.getAttribute(DefaultSubjectContext.PRINCIPALS_SESSION_KEY);
        if (null == obj) {
            return null;
        }

        // 确保是SimplePrincipalCollection对象
        if (obj instanceof SimplePrincipalCollection) {
            SimplePrincipalCollection spc = (SimplePrincipalCollection) obj;

            obj = spc.getPrimaryPrincipal();
            if (null != obj && obj instanceof UUser) {
                // 存储sesion+user综合信息
                UserOnlineBO onlineBO = new UserOnlineBO((UUser) obj);

                onlineBO.setLastAccessTime(session.getLastAccessTime());
                onlineBO.setHost(session.getHost());
                onlineBO.setSessionId(session.getId().toString());
                onlineBO.setLastTime(session.getLastAccessTime());
                onlineBO.setTimeout(session.getTimeout());
                onlineBO.setCreateTime(session.getStartTimestamp());

                // 是否踢出
                Long status = (Long) session.getAttribute(SESSION_STATUS);
                onlineBO.setStatus(status == null ? UUser.STATUS.VALID : status);
                return onlineBO;
            }
        }
        return null;
    }

}

package com.seer.shiro.core.session.impl;

import com.seer.shiro.core.cache.impl.CustomShiroCacheManager;
import com.seer.shiro.core.session.ShiroSessionRepository;
import org.apache.commons.lang3.SerializationUtils;
import org.apache.shiro.session.Session;

import java.io.Serializable;
import java.util.Collection;

/**
 * session管理实现类
 *
 * @author seer
 * @date 2017/9/19 22:08
 */
public class JedisShiroSessionRepository implements ShiroSessionRepository {
    public static final String REDIS_SHIRO_SESSION = "com-seer-session:";

    private static final int SESSION_VAL_TIME_SPAN = 18000;
    private static final int DB_INDEX = 1;

    @Override
    public void saveSession(Session session) {
        if (session == null || session.getId() == null) {
            throw new NullPointerException("sesson is empty");
        }

        try {
            byte[] key = SerializationUtils.serialize(buildRedisSessionKey(session.getId()));

            if (null == session.getAttribute(CustomShiroCacheManager.)) {

            }




        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void deleteSession(Serializable sessionId) {

    }

    @Override
    public Session getSession(Serializable sessionId) {
        return null;
    }

    @Override
    public Collection<Session> getAllSession() {
        return null;
    }

    /**
     * 构建session key
     * @param sessionId
     * @return
     */
    private String buildRedisSessionKey(Serializable sessionId) {
        return REDIS_SHIRO_SESSION + sessionId;
    }
}

package com.seer.shiro.core.session;

import org.apache.shiro.session.Session;

import java.io.Serializable;
import java.util.Collection;

/**
 * shiro的session 操作接口
 *
 * @author seer
 * @date 2017/9/19 21:54
 */
public interface ShiroSessionRepository {
    /**
     * 存储session
     *
     * @param session
     */
    void saveSession(Session session);

    /**
     * 删除session
     *
     * @param sessionId
     */
    void deleteSession(Serializable sessionId);

    /**
     * 获取session
     *
     * @param sessionId
     * @return
     */
    Session getSession(Serializable sessionId);

    /**
     * 获取所有的session
     *
     * @return
     */
    Collection<Session> getAllSession();
}

package com.seer.shiro.core.bean.bo;

import com.demo.model.PO.UUser;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户在线
 *
 * @author seer
 * @date 2017/10/9 21:52
 */
public class UserOnlineBO extends UUser implements Serializable {

    private static final long serialVersionUID = 6069051757143728496L;

    /**
     * sessionId
     */
    private String sessionId;

    /**
     * host
     */
    private String host;

    /**
     * session创建时间
     */
    private Date startTime;

    /**
     * session最后交互时间
     */
    private Date lastAccessTime;

    /**
     * 超时
     */
    private Long timeout;

    /**
     * session是否踢出
     */
    private Boolean sessionStatus = Boolean.TRUE;

    public UserOnlineBO() {
    }

    public UserOnlineBO(UUser uUser) {
        super(uUser);
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getLastAccessTime() {
        return lastAccessTime;
    }

    public void setLastAccessTime(Date lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }

    public Long getTimeout() {
        return timeout;
    }

    public void setTimeout(Long timeout) {
        this.timeout = timeout;
    }

    public Boolean getSessionStatus() {
        return sessionStatus;
    }

    public void setSessionStatus(Boolean sessionStatus) {
        this.sessionStatus = sessionStatus;
    }
}

package com.demo.util;

import org.apache.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
/*
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>javax.servlet-api</artifactId>
        <version>3.1.0</version>
    </dependency>
*/

/**
 * 解析HttpServletRequest请求的参数
 *
 * @author Seer
 * @date 2017/5/31 18:20
 */
public class RequestUtil {
    private static Logger log = Logger.getLogger(RequestUtil.class);

    /**
     * 获取所有request请求参数String
     *
     * @param request
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String getRequestParamsString(HttpServletRequest request) {
        try {
            int contentLength = request.getContentLength();
            if (contentLength < 0) {
                return null;
            }
            byte buffer[] = new byte[contentLength];
            for (int i = 0; i < contentLength; ) {

                int readlen = request.getInputStream().read(buffer, i, contentLength - i);
                if (readlen == -1) {
                    break;
                }
                i += readlen;
            }
            return new String(buffer, "utf8");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 获取所有request请求参数map
     *
     * @param request
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static Map<String, String> getRequestParamsMap(HttpServletRequest request) {
        Map<String, String> params = new HashMap<String, String>();
        try {
            if (null != request) {
                request.setCharacterEncoding("utf-8");
                Set<String> paramsKey = request.getParameterMap().keySet();
                for (String key : paramsKey) {

                    params.put(key, request.getParameter(key));
                }
            }
        } catch (UnsupportedEncodingException e) {
            log.error("获取所有request请求参数key-value发生异常：", e);
        }
        return params;
    }
}

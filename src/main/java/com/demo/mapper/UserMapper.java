package com.demo.mapper;

import com.demo.model.PO.SysPermissionPO;
import com.demo.model.PO.SysRolePO;
import com.demo.model.PO.SysUserPO;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * 用户信息Mapper
 *
 * @author seer
 * @date 2017/6/7 9:42
 */
@Repository
public interface UserMapper {

    /**
     * 根据id查询用户
     *
     * @param userId
     * @return
     */
    SysUserPO qryUserById(Long userId);

    /**
     * 根据用户名查询用户
     *
     * @param userName
     * @return
     */
    SysUserPO qryUserByUserName(String userName);

    /**
     * 根据用户名查询角色
     *
     * @param userName
     * @return
     */
    Set<String> listRoleByUserName(String userName);

    /**
     * 根据用户名查询权限
     *
     * @param userName
     * @return
     */
    Set<String> listPermissionsByUserName(String userName);

    /**
     * 增加用户
     *
     * @param sysUserPO
     * @return
     */
    int addUser(SysUserPO sysUserPO);

    /**
     * 更新用户
     *
     * @param sysUserPO
     */
    int updateUser(SysUserPO sysUserPO);

    /**
     * 删除用户
     *
     * @param userId
     */
    int removeUser(Long userId);

    /**
     * 增加角色
     *
     * @param sysRolePO
     * @return
     */
    int addRole(SysRolePO sysRolePO);

    /**
     * 删除角色
     *
     * @param roleId
     */
    int removeRole(Long roleId);

    /**
     * 增加权限
     *
     * @param sysPermissionPO
     * @return
     */
    int addPermission(SysPermissionPO sysPermissionPO);

    /**
     * 删除权限
     *
     * @param permissionId
     */
    int removePermission(Long permissionId);

    /**
     * 用户关联角色
     *
     * @param userId
     * @param roleIds
     * @return
     */
    int correlationRoles(@Param("userId") Long userId, @Param("roleIds") Long... roleIds);

    /**
     * 用户取消关联角色
     *
     * @param userId
     * @param roleIds
     * @return
     */
    int uncorrelationRoles(@Param("userId") Long userId, @Param("roleIds") Long... roleIds);

    /**
     * 角色关联权限
     *
     * @param roleId
     * @param permissionIds
     * @return
     */
    int correlationPermissions(@Param("roleId") Long roleId, @Param("permissionIds") Long... permissionIds);

    /**
     * 角色取消关联权限
     *
     * @param roleId
     * @param permissionIds
     * @return
     */
    int uncorrelationPermissions(@Param("roleId") Long roleId, @Param("permissionIds") Long... permissionIds);
}

package com.demo.controller;

import com.demo.model.PO.SysUserPO;
import com.demo.service.UserService;
import com.demo.util.RequestUtil;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * 用户Controller
 *
 * @author seer
 * @date 2017/6/7 16:13
 */
@Controller
@RequestMapping("/user")
public class UserController {
    private static Logger LOGGER = Logger.getLogger(UserController.class);

    @Autowired
    UserService userService;

    /**
     * 用户登录
     *
     * @return
     */
    @RequestMapping("/login")
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView("login");

        return modelAndView;
    }

    /**
     * 用户注册
     *
     * @return
     */
    @RequestMapping("/reg")
    public @ResponseBody
    Object register(SysUserPO sysUserPO) {
        LOGGER.info("... . . .-. 注册请求：" + sysUserPO);
        return null;
    }
}

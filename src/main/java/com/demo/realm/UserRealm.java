package com.demo.realm;

import com.demo.model.PO.SysUserPO;
import com.demo.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <Realm 实现>
 * <功能详细描述>
 *
 * @author seer
 * @version [V1.0, 2017/6/6]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class UserRealm extends AuthorizingRealm {

    @Autowired
    UserService userService;

    /**
     * 根据用户身份获取授权信息
     * AuthorizationInfo 授权器
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        String userName = (String) principalCollection.getPrimaryPrincipal();

        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.setRoles(userService.listRoleByUserName(userName));
        simpleAuthorizationInfo.setStringPermissions(userService.listPermissionsByUserName(userName));
        return simpleAuthorizationInfo;
    }

    /**
     * 获取身份验证信息
     * AuthenticationInfo 认证器
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        String userName = (String) authenticationToken.getPrincipal();
        SysUserPO sysUserPO = userService.qryUserByUserName(userName);

        if (sysUserPO == null) {
            // 没找到帐号
            throw new UnknownAccountException();
        }

        if (Boolean.TRUE.equals(sysUserPO.getLocked())) {
            // 帐号锁定
            throw new LockedAccountException();
        }

        // 凭证
        String credentialsSalt = sysUserPO.getUsername() + sysUserPO.getSalt();

        // 交给AuthenticatingRealm使用CredentialsMatcher进行密码匹配
        SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(
                sysUserPO.getUsername(),
                sysUserPO.getPassword(),
                ByteSource.Util.bytes(credentialsSalt),
                getName()
        );
        // 如果身份认证验证成功，返回一个 AuthenticationInfo 实现
        return simpleAuthenticationInfo;
    }
}

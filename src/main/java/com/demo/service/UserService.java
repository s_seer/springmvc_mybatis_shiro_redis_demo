package com.demo.service;

import com.demo.model.PO.SysUserPO;

import java.util.Set;

/**
 * 用户service
 *
 * @author seer
 * @date 2017/6/7 9:46
 */
public interface UserService {

    /**
     * 根据用户名查询用户
     *
     * @param userName
     * @return
     */
    SysUserPO qryUserByUserName(String userName);

    /**
     * 根据用户名查询角色
     *
     * @param userName
     * @return
     */
    Set<String> listRoleByUserName(String userName);

    /**
     * 根据用户名查询权限
     *
     * @param userName
     * @return
     */
    Set<String> listPermissionsByUserName(String userName);

    /**
     * 用户注册
     *
     * @param sysUserPO
     */
    Boolean register(SysUserPO sysUserPO);
}

package com.demo.service.impl;

import com.demo.service.UserService;
import com.demo.mapper.UserMapper;
import com.demo.model.PO.SysUserPO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

/**
 * 用户service实现
 *
 * @author seer
 * @date 2017/6/7 9:46
 */
@Service("userService")
public class UserServiceImpl implements UserService{
    private static Logger LOGGER = Logger.getLogger(UserServiceImpl.class);

    @Autowired
    UserMapper userMapper;

    @Override
    public SysUserPO qryUserByUserName(String userName) {
        return userMapper.qryUserByUserName(userName);
    }

    @Override
    public Set<String> listRoleByUserName(String userName) {
        return userMapper.listRoleByUserName(userName);
    }

    @Override
    public Set<String> listPermissionsByUserName(String userName) {
        return userMapper.listPermissionsByUserName(userName);
    }

    @Override
    @Transactional
    public Boolean register(SysUserPO sysUserPO) {
        userMapper.addUser(sysUserPO);
        return null;
    }
}

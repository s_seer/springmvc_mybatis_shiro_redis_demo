package com.demo.credentials;

import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheManager;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * <凭证匹配器>
 * <功能详细描述>
 *
 * @author seer
 * @version [V1.0, 2017/6/6]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class RetryLimitHashedCredentialsMatcher extends HashedCredentialsMatcher {
    private Cache<String, AtomicInteger> passwordRetryCache;

    public RetryLimitHashedCredentialsMatcher(CacheManager cacheManager) {
        passwordRetryCache = cacheManager.getCache("passwordRetryCache");
    }

    /**
     * 密码重试次数限制
     *
     * @param token
     * @param info
     * @return
     */
    @Override
    public boolean doCredentialsMatch(AuthenticationToken token, AuthenticationInfo info) {
        /**
         * 如在 1 个小时内密码最多重试 5 次，如果尝试次数超过 5 次就锁定 1 小时，1 小时后可再
         次重试，如果还是重试失败，可以锁定如 1 天，以此类推，防止密码被暴力破解。我们通
         过继承 HashedCredentialsMatcher，且使用 Ehcache 记录重试次数和超时时间。
         */
        String userName = (String) token.getPrincipal();

        AtomicInteger retryCount = passwordRetryCache.get(userName);

        if (retryCount == null) {
            retryCount = new AtomicInteger(0);
            passwordRetryCache.put(userName, retryCount);
        }

        if (retryCount.incrementAndGet() > 5) {
            throw new ExcessiveAttemptsException();
        }

        // 校验通过，从缓存中移除
        boolean matches = super.doCredentialsMatch(token, info);
        if (matches) {
            passwordRetryCache.remove(userName);
        }

        return matches;
    }
}

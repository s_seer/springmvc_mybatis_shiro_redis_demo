package com.demo.model.PO;

import java.io.Serializable;

/**
 * 角色表
 *
 * @author seer
 * @date 2017/6/7 9:24
 */
public class SysRolePO implements Serializable {

    private static final long serialVersionUID = -7109544501831368229L;

    /**
     * id
     */
    private Long id;

    /**
     * 角色标识
     */
    private String role;

    /**
     * 角色描述
     */
    private String description;

    /**
     * 是否可用,如果不可用将不会添加给用户
     */
    private Boolean available = Boolean.FALSE;

    public SysRolePO() {
    }

    public SysRolePO(String role, String description, Boolean available) {
        this.role = role;
        this.description = description;
        this.available = available;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysRolePO sysRolePO = (SysRolePO) o;

        if (id != null ? !id.equals(sysRolePO.id) : sysRolePO.id != null) return false;
        if (role != null ? !role.equals(sysRolePO.role) : sysRolePO.role != null) return false;
        if (description != null ? !description.equals(sysRolePO.description) : sysRolePO.description != null)
            return false;
        return available != null ? available.equals(sysRolePO.available) : sysRolePO.available == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (role != null ? role.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (available != null ? available.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SysRolePO{" +
                "id=" + id +
                ", role='" + role + '\'' +
                ", description='" + description + '\'' +
                ", available=" + available +
                '}';
    }
}


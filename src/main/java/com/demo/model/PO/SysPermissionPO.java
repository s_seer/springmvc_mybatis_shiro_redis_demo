package com.demo.model.PO;

import java.io.Serializable;

/**
 * 权限表
 *
 * @author seer
 * @date 2017/6/7 9:27
 */
public class SysPermissionPO implements Serializable {
    private static final long serialVersionUID = 381673429942450100L;

    /**
     * id
     */
    private Long id;

    /**
     * 权限标识
     */
    private String permission;

    /**
     * 权限描述
     */
    private String description;

    /**
     * 是否可用,如果不可用将不会添加给用户
     */
    private Boolean available = Boolean.FALSE;

    public SysPermissionPO() {
    }

    public SysPermissionPO(String permission, String description, Boolean available) {
        this.permission = permission;
        this.description = description;
        this.available = available;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysPermissionPO that = (SysPermissionPO) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (permission != null ? !permission.equals(that.permission) : that.permission != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        return available != null ? available.equals(that.available) : that.available == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (permission != null ? permission.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (available != null ? available.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SysPermissionPO{" +
                "id=" + id +
                ", permission='" + permission + '\'' +
                ", description='" + description + '\'' +
                ", available=" + available +
                '}';
    }
}



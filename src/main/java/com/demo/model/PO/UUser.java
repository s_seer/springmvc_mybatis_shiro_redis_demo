package com.demo.model.PO;

import java.io.Serializable;
import java.util.Date;

public class UUser implements Serializable {
    private static final long serialVersionUID = -4399370027599936695L;

    private Long id;

    /**
     * 昵称
     */
    private String nickname;

    /**
     * 邮箱，登录帐号
     */
    private String email;

    /**
     * 密码
     */
    private transient String password;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 最后登录时间
     */
    private Date lastTime;

    /**
     * 状态
     * 0：禁止登录；1：有效
     */
    private Long status;

    public UUser() {
    }

    public UUser(UUser user) {
        this.id = user.id;
        this.nickname = user.nickname;
        this.email = user.email;
        this.password = user.password;
        this.createTime = user.createTime;
        this.lastTime = user.lastTime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getLastTime() {
        return lastTime;
    }

    public void setLastTime(Date lastTime) {
        this.lastTime = lastTime;
    }

    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }

    public interface STATUS {
        /**
         * 有效
         */
        Long VALID = 1L;

        /**
         * 失效
         */
        Long INVALID = 0L;
    }
}

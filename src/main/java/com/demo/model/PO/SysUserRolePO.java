package com.demo.model.PO;

import java.io.Serializable;

/**
 * 用户角色关系
 *
 * @author seer
 * @date 2017/6/7 9:36
 */
public class SysUserRolePO implements Serializable {
    private static final long serialVersionUID = -6800178487535591352L;

    /**
     * 用户id
     */
    private Long user_id;

    /**
     * 角色id
     */
    private Long role_id;

    public SysUserRolePO() {
    }

    public SysUserRolePO(Long user_id, Long role_id) {
        this.user_id = user_id;
        this.role_id = role_id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public Long getRole_id() {
        return role_id;
    }

    public void setRole_id(Long role_id) {
        this.role_id = role_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysUserRolePO that = (SysUserRolePO) o;

        if (user_id != null ? !user_id.equals(that.user_id) : that.user_id != null) return false;
        return role_id != null ? role_id.equals(that.role_id) : that.role_id == null;
    }

    @Override
    public int hashCode() {
        int result = user_id != null ? user_id.hashCode() : 0;
        result = 31 * result + (role_id != null ? role_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SysUserRolePO{" +
                "user_id=" + user_id +
                ", role_id=" + role_id +
                '}';
    }
}

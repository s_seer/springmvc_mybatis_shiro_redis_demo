package com.demo.model.PO;

import java.io.Serializable;

/**
 * 角色权限关系
 *
 * @author seer
 * @date 2017/6/7 9:36
 */
public class SysRolePermissionPO implements Serializable {
    private static final long serialVersionUID = 9194189863585598117L;

    /**
     * 角色id
     */
    private Long role_id;

    /**
     * 权限id
     */
    private Long permission_id;

    public SysRolePermissionPO() {
    }

    public SysRolePermissionPO(Long role_id, Long permission_id) {
        this.role_id = role_id;
        this.permission_id = permission_id;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getRole_id() {
        return role_id;
    }

    public void setRole_id(Long role_id) {
        this.role_id = role_id;
    }

    public Long getPermission_id() {
        return permission_id;
    }

    public void setPermission_id(Long permission_id) {
        this.permission_id = permission_id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysRolePermissionPO that = (SysRolePermissionPO) o;

        if (role_id != null ? !role_id.equals(that.role_id) : that.role_id != null) return false;
        return permission_id != null ? permission_id.equals(that.permission_id) : that.permission_id == null;
    }

    @Override
    public int hashCode() {
        int result = role_id != null ? role_id.hashCode() : 0;
        result = 31 * result + (permission_id != null ? permission_id.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SysRolePermissionPO{" +
                "role_id=" + role_id +
                ", permission_id=" + permission_id +
                '}';
    }
}

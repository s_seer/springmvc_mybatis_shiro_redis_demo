package com.demo.model.PO;

import java.io.Serializable;

/**
 * <用户表>
 * <功能详细描述>
 *
 * @author seer
 * @version [V1.0, 2017/6/6]
 * @see [相关类/方法]
 * @since [产品/模块版本]
 */
public class SysUserPO implements Serializable {
    private static final long serialVersionUID = -182798967061155863L;

    /**
     * id
     */
    private Long id;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 盐
     */
    private String salt;

    /**
     * 锁定
     */
    private Boolean locked = Boolean.FALSE;

    public SysUserPO() {
    }

    public SysUserPO(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysUserPO sysUserPO = (SysUserPO) o;

        if (id != null ? !id.equals(sysUserPO.id) : sysUserPO.id != null) return false;
        if (username != null ? !username.equals(sysUserPO.username) : sysUserPO.username != null) return false;
        if (password != null ? !password.equals(sysUserPO.password) : sysUserPO.password != null) return false;
        if (salt != null ? !salt.equals(sysUserPO.salt) : sysUserPO.salt != null) return false;
        return locked != null ? locked.equals(sysUserPO.locked) : sysUserPO.locked == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (salt != null ? salt.hashCode() : 0);
        result = 31 * result + (locked != null ? locked.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SysUserPO{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", salt='" + salt + '\'' +
                ", locked=" + locked +
                '}';
    }
}
